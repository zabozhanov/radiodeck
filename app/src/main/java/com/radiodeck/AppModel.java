package com.radiodeck;

import android.app.Application;

import com.radiodeck.net.ApiService;
import retrofit.RestAdapter;

/**
 * Created by zabozhanov on 27/10/14.
 */
public class AppModel extends Application {
    protected static AppModel instance;

    public ApiService apiService;

    public String getLogoUrl(String id, boolean hasLogo, boolean x2) {
        if (hasLogo) {
            if (x2) {
                return String.format("http://images.radiodeck.com/radio/%1s/logo/160x160.jpg", id);
            } else {
                return String.format("http://images.radiodeck.com/radio/%1s/logo/80x80.jpg", id);
            }
        } else {
            if (x2) {
                return "http://images.radiodeck.com/media/radio/nologo/160x160.jpg";
            } else {
                return "http://images.radiodeck.com/media/radio/nologo/80x80.jpg";
            }
        }
    }

    public String getCoverLogoUrl(String id, boolean hasLogo) {
        if (hasLogo) {
            return String.format("http://images.radiodeck.com/radio/%1s/logo/160x160.jpg", id);
        } else {
            return "http://images.radiodeck.com/media/radio/nologo/160x160.jpg";
        }
    }

    public String getCampaignUrl(String id) {
        return "http://images.radiodeck.com/campaigns/l/" + id + ".jpg";
    }


    public static AppModel getInstance() {
        if (instance == null) {
            AppModel appModel = new AppModel();
            appModel.onCreate();
        }
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint("http://www.radiodeck.com/")
                //.setEndpoint("http://arny.dev.radiodeck.com/")
                .build();
        apiService = adapter.create(ApiService.class);
    }
}
