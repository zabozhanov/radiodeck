package com.radiodeck;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.radiodeck.net.responses.BrowseResponse;
import com.radiodeck.views.MenuRecycleView;


public class BrowseActivity extends FragmentActivity {

    protected MenuRecycleView recyclerView;
    protected BrowseAdapter recyclerAdapter;
    protected SlidingMenu slidingMenu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browse);

        recyclerView = (MenuRecycleView) findViewById(R.id.recyclerview);
        recyclerAdapter = new BrowseAdapter(this, recyclerView, getSupportFragmentManager());
        //recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(recyclerAdapter);


        slidingMenu = new SlidingMenu(this);
        slidingMenu.setMode(SlidingMenu.LEFT);
        slidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
        slidingMenu.setShadowWidthRes(R.dimen.shadow_width);
        slidingMenu.setShadowDrawable(R.drawable.shadow);
        slidingMenu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        slidingMenu.setFadeDegree(0.35f);
        slidingMenu.attachToActivity(this, SlidingMenu.SLIDING_WINDOW);
        slidingMenu.setMenu(R.layout.left_menu);

        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayShowCustomEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        DataLoader loader = new DataLoader();
        loader.execute();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_discover, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == android.R.id.home) {
            menuToggle();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void menuToggle(){
        if(slidingMenu.isMenuShowing())
            slidingMenu.showContent();
        else
            slidingMenu.showMenu();
    }

    protected class DataLoader extends AsyncTask<Void, Void, BrowseResponse> {
        @Override
        protected BrowseResponse doInBackground(Void... voids) {
            try {
                BrowseResponse response = AppModel.getInstance().apiService.getBrowse();
                return response;
            } catch (Exception ex) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(BrowseResponse browseResponse) {
            super.onPostExecute(browseResponse);
            if (browseResponse != null) {
                recyclerAdapter.updateWithData(browseResponse, false);
            }
        }
    }
}
