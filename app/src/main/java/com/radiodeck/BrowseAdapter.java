package com.radiodeck;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.oguzdev.circularfloatingactionmenu.library.FloatingActionMenu;
import com.oguzdev.circularfloatingactionmenu.library.SubActionButton;
import com.radiodeck.net.responses.BrowseResponse;
import com.radiodeck.net.responses.DiscoverCampaign;
import com.radiodeck.net.responses.radio.Country;
import com.radiodeck.net.responses.radio.Genre;
import com.radiodeck.net.responses.radio.GenriesItem;
import com.radiodeck.net.responses.radio.LocationItem;
import com.radiodeck.net.responses.radio.RadioRecommendations;
import com.radiodeck.views.CampaignFragment;
import com.radiodeck.views.MenuRecycleView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import com.radiodeck.net.responses.DiscoverData;
import com.radiodeck.net.responses.Station;

import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;

/**
 * Created by zabozhanov on 26/10/14.
 */
public class BrowseAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  implements FloatingActionMenu.MenuStateChangeListener{

    protected BrowseResponse data;
    protected List<RecycleItem> recycleItems;
    protected Context context;
    protected MenuRecycleView recyclerView;
    protected boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    protected FragmentManager fragmentManager;
    protected Pager pager;

    enum ITEM_TYPES {
        ITEM_TYPE_LIST_FOOTER, //listview footer
        ITEM_TYPE_PLUS_PLAY, //plus-minus item
        ITEM_TYPE_LIST_ITEM, //station item
        ITEM_TYPE_GENRE_LOCATION, //genre-location item
        ITEM_TYPE_COVER_LIST, //hor list
        ITEM_TYPE_LIST_HEADER, //section header
        ITEM_TYPE_CAMPAIGN, //section header
        ITEM_TYPE_LOADING_FOOTER
    }

    class RecycleItem {
        public ITEM_TYPES type;
        public String header;
        public Station station;
        public Genre genre;
        public Country location;
        public boolean isLastItem;
        public boolean isFirstItem;

        public HorizontalRecyclerViewAdapter horizontalRecyclerViewAdapter;

        RecycleItem(ITEM_TYPES type, String header) {
            this.type = type;
            this.header = header;
        }

        RecycleItem(ITEM_TYPES type) {
            this.type = type;
        }
    }

    public BrowseAdapter(Context context, MenuRecycleView recyclerView, FragmentManager fragmentManager) {
        this.context = context;
        this.recyclerView = recyclerView;
        recycleItems = new ArrayList<RecycleItem>();
        this.fragmentManager = fragmentManager;
        data = new BrowseResponse();
        pager = new Pager(fragmentManager);

        SubActionButton.Builder rLSubBuilder = new SubActionButton.Builder(context);
        ImageView rlIcon1 = new ImageView(context);
        ImageView rlIcon2 = new ImageView(context);

        rlIcon1.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_button_plus));
        rlIcon2.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_button_play));

        View view = new View(context);
        recyclerView.menu = new FloatingActionMenu.Builder(context)
                /*.setStartAngle(-45)
                .setEndAngle(-135)*/
                .setRadius(context.getResources().getDimensionPixelSize(R.dimen.radius_medium))
                .addSubActionView(rLSubBuilder.setContentView(rlIcon1).build())
                .addSubActionView(rLSubBuilder.setContentView(rlIcon2).build())
                        // listen state changes of each menu
                .setStateChangeListener(this)
                .attachTo(view)
                .build();

        /*recyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    *//*if (menu != null && menu.isOpen())
                    menu.close(true);*//*
                }
                return false;
            }
        });*/


    }

    public void updateWithData(BrowseResponse data, boolean updateModel) {

        if (data == null) {
            return;
        }
        if (!updateModel) {
            this.data = data;
            recycleItems = new ArrayList<RecycleItem>();
        }

        if (recyclerView != null) {
            recyclerView.setOnScrollListener(onScrollListener);
        }

        Gson gson = new Gson();
        if (!updateModel) {

            if (data.extra.campaignsTop.list.size() > 0) {
                recycleItems.add(new RecycleItem(ITEM_TYPES.ITEM_TYPE_CAMPAIGN));
            }

            for (JsonObject recommendation : data.extra.moreData) {
                if (recommendation.get("type").toString().equals("\"STATION\"")) {
                    RadioRecommendations recommendations = gson.fromJson(recommendation.toString(), RadioRecommendations.class);
                    if (recommendations.display.equals("LIST")) {
                        recycleItems.add(new RecycleItem(ITEM_TYPES.ITEM_TYPE_LIST_HEADER, recommendations.title));
                        for (int i = 0; i < recommendations.list.size(); i++) {
                            RecycleItem item = new RecycleItem(ITEM_TYPES.ITEM_TYPE_LIST_ITEM);
                            item.station = recommendations.list.get(i);
                            recycleItems.add(item);
                        }
                        recycleItems.get(recycleItems.size() - 1).isLastItem = true;
                    } else {
                        RecycleItem item = new RecycleItem(ITEM_TYPES.ITEM_TYPE_COVER_LIST);
                        item.header = recommendations.title;
                        item.horizontalRecyclerViewAdapter = new HorizontalRecyclerViewAdapter(context, recyclerView);
                        item.horizontalRecyclerViewAdapter.updateWithData(recommendations.list);
                        recycleItems.add(item);
                    }
                } else if (recommendation.get("type").toString().equals("\"GENRE\"")) {
                    GenriesItem genries = gson.fromJson(recommendation.toString(), GenriesItem.class);
                    recycleItems.add(new RecycleItem(ITEM_TYPES.ITEM_TYPE_LIST_HEADER, genries.title));
                    for (int i = 0; i < genries.list.size(); i++) {
                        RecycleItem item = new RecycleItem(ITEM_TYPES.ITEM_TYPE_GENRE_LOCATION);
                        if (i == 0) item.isFirstItem = true;
                        item.genre = genries.list.get(i);
                        recycleItems.add(item);
                    }
                    recycleItems.get(recycleItems.size() - 1).isLastItem = true;
                } else if (recommendation.get("type").toString().equals("\"LOCATION\"")) {
                    LocationItem locationItem = gson.fromJson(recommendation.toString(), LocationItem.class);
                    recycleItems.add(new RecycleItem(ITEM_TYPES.ITEM_TYPE_LIST_HEADER, locationItem.title));
                    for (int i = 0; i < locationItem.list.size(); i++) {
                        RecycleItem item = new RecycleItem(ITEM_TYPES.ITEM_TYPE_GENRE_LOCATION);
                        if (i == 0) item.isFirstItem = true;
                        item.location = locationItem.list.get(i);
                        recycleItems.add(item);
                    }
                    recycleItems.get(recycleItems.size() - 1).isLastItem = true;
                }
            }
        }

        if (data.model != null && data.model.list.size() > 0) {
            if (!updateModel) {
                recycleItems.add(new RecycleItem(ITEM_TYPES.ITEM_TYPE_LIST_HEADER, "Music"));
            } else {
                if (recycleItems.get(recycleItems.size() - 1).type == ITEM_TYPES.ITEM_TYPE_LOADING_FOOTER) {
                    recycleItems.remove(recycleItems.size() - 1);
                }
            }
            for (Station station : data.model.list) {
                RecycleItem item = new RecycleItem(ITEM_TYPES.ITEM_TYPE_LIST_ITEM);
                item.station = station;
                recycleItems.add(item);
            }
            //recycleItems.get(recycleItems.size() - 1).isLastItem = true;
            recycleItems.add(new RecycleItem(ITEM_TYPES.ITEM_TYPE_LOADING_FOOTER));
        } else {
            recycleItems.add(new RecycleItem(ITEM_TYPES.ITEM_TYPE_LIST_FOOTER));
        }

        this.notifyDataSetChanged();
    }


    protected RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            LinearLayoutManager mLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

            visibleItemCount = mLayoutManager.getChildCount();
            totalItemCount = mLayoutManager.getItemCount();
            pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

            if (loading) {
                if ( (visibleItemCount+pastVisiblesItems) >= totalItemCount) {
                    loading = false;

                    if (data.model.next == null || data.model.next.isEmpty()) {
                        recycleItems.remove(recycleItems.size() - 1); //removing loading item
                        recycleItems.add(new RecycleItem(ITEM_TYPES.ITEM_TYPE_LIST_FOOTER));
                        notifyDataSetChanged();
                        return;
                    }

                    Log.d("INFINIITY", "loading start: " + data.model.next);
                    new NextPageLoader().execute();

                }
            }

            /*int visibleThreshold = 1;
            if (!loading) {
                if ((totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {


                    loading = true;
                }
            }*/
        }
    };

    protected class NextPageLoader extends AsyncTask<String, Void, BrowseResponse> {

        @Override
        protected BrowseResponse doInBackground(String... strings) {
            try {
                return AppModel.getInstance().apiService.getMusicNext(data.model.next);
            } catch (Exception ex) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(BrowseResponse browseResponse) {
            super.onPostExecute(browseResponse);
            if (browseResponse != null) {
                data.model.next = browseResponse.model.next;
                updateWithData(browseResponse, true);
            }
            loading = true;
        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        if (i == ITEM_TYPES.ITEM_TYPE_LIST_HEADER.ordinal()) { //stations header row
            return new ViewHolderHeader(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_station_header_with_divider, viewGroup, false));
        } else if (i == ITEM_TYPES.ITEM_TYPE_LIST_ITEM.ordinal()) { //stations item row
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_station_list_item, viewGroup, false);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int i = recyclerView.getChildPosition(view);
                    RecycleItem item = recycleItems.get(i);
                    Intent intent = new Intent(context, RadioStationActivity.class);
                    intent.putExtra("id", item.station._id);
                    context.startActivity(intent);
                }
            });
            return new ViewHolderListStation(v);

        } else if (i == ITEM_TYPES.ITEM_TYPE_COVER_LIST.ordinal()) { //horizontal list
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_cover_listview, viewGroup, false);
            return new ViewHolderCoverItem(v);
        } else if (i == ITEM_TYPES.ITEM_TYPE_LIST_FOOTER.ordinal()) { //footer
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_footer, viewGroup, false);
            v.setBackgroundColor(context.getResources().getColor(R.color.gray_background));
            return new ViewHolder(v);
        } else if (i == ITEM_TYPES.ITEM_TYPE_GENRE_LOCATION.ordinal()) { //country location
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_country_genry, viewGroup, false);
            return new ViewHolderGenreLocation(v);
        } else if (i == ITEM_TYPES.ITEM_TYPE_LOADING_FOOTER.ordinal()) {
            return new ViewHolderHeader(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_loading_footer, viewGroup, false));
        } else if (i == ITEM_TYPES.ITEM_TYPE_CAMPAIGN.ordinal()){
            return new ViewHolderCampaign(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_campaign_container, viewGroup, false));
        }
        else {
            return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_station_list_item, viewGroup, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        RecycleItem item = recycleItems.get(position);
        if (item.type == ITEM_TYPES.ITEM_TYPE_LIST_FOOTER) { //plus play

        } else if (item.type == ITEM_TYPES.ITEM_TYPE_LIST_HEADER) { //station heaader
            ViewHolderHeader viewHolderHeader = (ViewHolderHeader) viewHolder;
            viewHolderHeader.textView.setText(item.header);
        } else if (item.type == ITEM_TYPES.ITEM_TYPE_COVER_LIST) {
            ViewHolderCoverItem viewHolderCoverItem = (ViewHolderCoverItem) viewHolder;
            viewHolderCoverItem.txtTitle.setText(item.header);
            if (viewHolderCoverItem.hListView.getAdapter() == null) {
                viewHolderCoverItem.hListView.setAdapter(item.horizontalRecyclerViewAdapter);
                viewHolderCoverItem.hListView.getAdapter().notifyDataSetChanged();
            }
        } else if (item.type == ITEM_TYPES.ITEM_TYPE_GENRE_LOCATION) { //station heaader
            ViewHolderGenreLocation viewHolderGenreLocation = (ViewHolderGenreLocation) viewHolder;
            if (item.location != null) {
                viewHolderGenreLocation.txtTitle.setText(item.location.label);
            } else if (item.genre != null) {
                viewHolderGenreLocation.txtTitle.setText(item.genre.label);
            }
            viewHolderGenreLocation.dividerView.setVisibility(item.isLastItem ? View.INVISIBLE : View.VISIBLE);
            float scale = context.getResources().getDisplayMetrics().density;
            int dpAsPixels = (int) ((item.isFirstItem ? 0 : 10) * scale + 0.5f);
            viewHolderGenreLocation.txtTitle.setPadding(0, dpAsPixels, 0, (int) (10 * scale + 0.5f));
        } else if (item.type == ITEM_TYPES.ITEM_TYPE_LOADING_FOOTER) {

        } else if (item.type == ITEM_TYPES.ITEM_TYPE_CAMPAIGN) {
            ViewHolderCampaign viewHolderCampaign = (ViewHolderCampaign) viewHolder;
            viewHolderCampaign.discoverCampaigns = data.extra.campaignsTop.list;
            viewHolderCampaign.update();
        }
        else {
            ViewHolderListStation viewHolderListStation = (ViewHolderListStation) viewHolder;
            viewHolderListStation.txtSubTitle.setText(item.station.infoLine);
            viewHolderListStation.txtTitle.setText(item.station.base.name);
            Picasso.with(context).load(AppModel.getInstance().
                    getLogoUrl(item.station._id, item.station.base.hasLogo, false))
                    .into(viewHolderListStation.imgView);
            viewHolderListStation.dividerView.setVisibility(item.isLastItem ? View.INVISIBLE : View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return recycleItems.size(); //lists and headers
    }

    @Override
    public int getItemViewType(int position) {
        return recycleItems.get(position).type.ordinal();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View itemView) {
            super(itemView);
            //txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
        }
    }

    class ViewHolderCampaign extends RecyclerView.ViewHolder {
        private AutoScrollViewPager viewPager;
        public List<DiscoverCampaign> discoverCampaigns;

        public void update() {
            pager.notifyDataSetChanged();
        }

        public ViewHolderCampaign(View itemView) {
            super(itemView);
            viewPager = (AutoScrollViewPager) itemView.findViewById(R.id.viewPager);
            viewPager.setAdapter(pager);

            viewPager.setInterval(5000);
            viewPager.setAutoScrollDurationFactor(2);
            viewPager.startAutoScroll();
        }

    }

    class ViewHolderHeader extends RecyclerView.ViewHolder {
        private TextView textView;

        public ViewHolderHeader(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.textview);
        }
    }

    class ViewHolderListStation extends RecyclerView.ViewHolder {
        private TextView txtTitle;
        private TextView txtSubTitle;
        private ImageView imgView;
        private View dividerView;
        private View touchView;


        public ViewHolderListStation(final View itemView) {
            super(itemView);

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {

                    int width = view.getMeasuredWidth();
                    width /= 2;
                    if (touchView.getLeft() < width) {
                        recyclerView.menu.setStartAngle(270);
                        recyclerView.menu.setEndAngle(-20);
                    } else {
                        recyclerView.menu.setStartAngle(200);
                        recyclerView.menu.setEndAngle(270);
                    }

                    recyclerView.menu.setMainActionView(touchView);
                    recyclerView.menu.open(true);

                    return true;
                }
            });
            itemView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {

                    final int action = motionEvent.getAction();
                    switch (action & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_DOWN: {
                            final int x = (int) motionEvent.getX();
                            final int y = (int) motionEvent.getY();
                            touchView.setLeft(x);
                            touchView.setTop(y);
                            return false;
                        }
                    }
                    return false;
                }
            });

            touchView = new View(context);
            touchView.setVisibility(View.INVISIBLE);
            ((ViewGroup)itemView).addView(touchView);
            txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
            txtSubTitle = (TextView) itemView.findViewById(R.id.txtSubTitle);
            imgView = (ImageView) itemView.findViewById(R.id.imgView);
            dividerView = itemView.findViewById(R.id.horizontaldivider);
        }
    }

    class ViewHolderCoverItem extends RecyclerView.ViewHolder {
        private TextView txtTitle;
        private RecyclerView hListView;
        private LinearLayoutManager layoutManager;

        public ViewHolderCoverItem(View itemView) {
            super(itemView);
            txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
            hListView = (RecyclerView) itemView.findViewById(R.id.horizontalrecycler);
            layoutManager = new LinearLayoutManager(itemView.getContext());
            layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            hListView.setLayoutManager(layoutManager);
        }
    }

    class ViewHolderGenreLocation extends RecyclerView.ViewHolder {
        private TextView txtTitle;
        private View dividerView;

        public ViewHolderGenreLocation(View itemView) {
            super(itemView);
            txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
            dividerView = itemView.findViewById(R.id.horizontaldivider);
        }
    }


    protected class Pager extends FragmentPagerAdapter {
        protected List<CampaignFragment> fragments = new ArrayList<CampaignFragment>();


        public Pager(FragmentManager fm) {
            super(fm);
        }

        public void update() {
                /*fragments = new ArrayList<CampaignFragment>();
                for (DiscoverCampaign campaign : discoverCampaigns) {
                    CampaignFragment fragment = CampaignFragment.newInstance(
                            AppModel.getInstance().getCampaignUrl(campaign.id));
                    //fragments.add(fragment);
                }*/
            notifyDataSetChanged();
        }

        @Override
        public Fragment getItem(int i) {
            //return fragments.get(i);
            return CampaignFragment.newInstance(
                    AppModel.getInstance().getCampaignUrl(data.extra.campaignsTop.list.get(i).id));
        }

        @Override
        public int getCount() {
            return data.extra.campaignsTop.list.size();
        }
    }


    @Override
    public void onMenuOpened(FloatingActionMenu menu) {

    }

    @Override
    public void onMenuClosed(FloatingActionMenu menu) {

    }
}
