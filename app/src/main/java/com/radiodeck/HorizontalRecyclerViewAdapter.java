package com.radiodeck;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import com.radiodeck.net.responses.DiscoverData;
import com.radiodeck.net.responses.Station;

/**
 * Created by zabozhanov on 28/10/14.
 */
public class HorizontalRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {
    protected List<Station> data;
    protected Context context;
    protected RecyclerView recyclerView;

    public HorizontalRecyclerViewAdapter(Context context, RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
        this.context = context;
        data = new ArrayList<Station>();
    }

    public void updateWithData(List<Station> data) {
        this.data = data;
        this.notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cover_station_item, viewGroup, false);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int i = recyclerView.getChildPosition(view);
                Station station = data.get(i);
                Intent intent = new Intent(context, RadioStationActivity.class);
                intent.putExtra("id", station._id);
                context.startActivity(intent);
            }
        });
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Station station = data.get(position);
        ViewHolder viewHolder = (ViewHolder)holder;

        Picasso.with(context).
                load(AppModel.getInstance().getCoverLogoUrl(station._id, station.base.hasLogo))
                //.centerInside()
                .into(viewHolder.imgView);
        viewHolder.txtTitle.setText(station.base.name);
        viewHolder.txtSubTitle.setText(station.infoLine);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgView;
        private TextView txtTitle;
        private TextView txtSubTitle;

        public ViewHolder(View itemView) {
            super(itemView);
            imgView = (ImageView) itemView.findViewById(R.id.imgView);
            txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
            txtSubTitle = (TextView) itemView.findViewById(R.id.txtSubTitle);
        }
    }

}
