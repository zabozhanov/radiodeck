package com.radiodeck;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;


public class PlayerActivity extends Activity {
    protected View actionBarView;
    private TextView txtTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        getActionBar().setBackgroundDrawable(null);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getActionBar().setCustomView(R.layout.radio_actionbar);

        actionBarView = getActionBarView();
        txtTitle = (TextView)((ViewGroup) actionBarView).findViewById(R.id.txtTitle);
        TextView txtBack = (TextView)((ViewGroup) actionBarView).findViewById(R.id.txtBack);
        txtBack.setVisibility(View.GONE);

        ((ViewGroup) actionBarView).findViewById(R.id.btnBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        final ImageView imgLogo = (ImageView) findViewById(R.id.imgLogo);
        String radioId = getIntent().getStringExtra("id");
        Picasso.with(this).load(AppModel.getInstance().
                getCoverLogoUrl(radioId, !radioId.isEmpty()))
                .into(imgLogo);
        txtTitle.setText(getIntent().getStringExtra("title"));
    }

    public View getActionBarView() {
        Window window = getWindow();
        View v = window.getDecorView();
        int resId = getResources().getIdentifier("action_bar_container", "id", "android");
        return v.findViewById(resId);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
