package com.radiodeck;

import android.app.ActionBar;
import android.app.Activity;
import android.graphics.drawable.TransitionDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import com.radiodeck.net.responses.radio.RadioResponse;
import com.radiodeck.views.CustomRelativeWrapper;
import com.radiodeck.views.ParallaxRecycleAdapter;
import com.radiodeck.views.ParallaxRecyclerView;


public class RadioStationActivity extends Activity {

    protected ParallaxRecyclerView recyclerView;
    protected ParallaxRecycleAdapter recyclerAdapter;
    protected int actionBarHeightInPixels;
    protected boolean actionBarHidden;
    protected View actionBarView;
    private TextView txtTitle;
    private TransitionDrawable transition;
    private RadioResponse radioResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radio_station);


        recyclerView = (ParallaxRecyclerView) findViewById(R.id.recyclerview);
        recyclerAdapter = new ParallaxRecycleAdapter(this, recyclerView);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(recyclerAdapter);
        recyclerView.scrollListener = this.scrollListener;
        recyclerAdapter.changeSizeHeaderImageView = (ImageView) findViewById(R.id.imgView);

        recyclerAdapter.mHeader = (CustomRelativeWrapper) findViewById(R.id.headerView);

        try {
            getActionBar().setBackgroundDrawable(null);
            getActionBar().setTitle("");
            TypedValue tv = new TypedValue();
            if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true))
            {
                actionBarHeightInPixels = TypedValue.complexToDimensionPixelSize(tv.data,getResources().getDisplayMetrics());
            }
            getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            getActionBar().setCustomView(R.layout.radio_actionbar);

            actionBarView = getActionBarView();
            txtTitle = (TextView)((ViewGroup) actionBarView).findViewById(R.id.txtTitle);
            actionBarView.setBackgroundResource(R.drawable.opacity_actionbar);
            transition = (TransitionDrawable) actionBarView.getBackground();

            ((ViewGroup) actionBarView).findViewById(R.id.btnBack).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
            ((ViewGroup) actionBarView).findViewById(R.id.txtBack).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });

        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();


        String id = getIntent().getStringExtra("id");
        if (id != null && !id.isEmpty()) {
            new DataLoader().execute(id);
        }
    }

    public View getActionBarView() {
        Window window = getWindow();
        View v = window.getDecorView();
        int resId = getResources().getIdentifier("action_bar_container", "id", "android");
        return v.findViewById(resId);
    }

    protected RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            ParallaxRecycleAdapter.ViewHolderParallaxHeader holder =
                    (ParallaxRecycleAdapter.ViewHolderParallaxHeader)recyclerView.findViewHolderForPosition(0);

            if (holder != null) {
                int bottom = holder.txtSubTitle.getBottom() + holder.itemView.getTop();
                if (bottom < actionBarHeightInPixels) {
                    if (!actionBarHidden) {
                        actionBarHidden = true;
                        transition.startTransition(150);
                        txtTitle.setText(radioResponse.model.base.name);
                    }
                } else {
                    if (actionBarHidden) {
                        actionBarHidden = false;
                        transition.reverseTransition(150);
                        txtTitle.setText("");
                    }
                }
            }
        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu_radio_station, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        } else if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    protected class DataLoader extends AsyncTask<String, Void, RadioResponse> {
        @Override
        protected RadioResponse doInBackground(String... voids) {
            try {
                return AppModel.getInstance().apiService.getRadio(voids[0]);
            } catch (Exception ex) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(RadioResponse data) {
            super.onPostExecute(data);
            if (data != null) {
                RadioStationActivity.this.radioResponse = data;
                recyclerAdapter.updateWithData(data);

                Picasso.with(RadioStationActivity.this).load("http://images.radiodeck.com/media/cover/c1.jpg")
                        .noFade()
                        .into((ImageView) findViewById(R.id.imgView));
                Picasso.with(RadioStationActivity.this).load(AppModel.getInstance().
                        getLogoUrl(data.model._id, data.model.base.hasLogo, false))
                        .into(((ImageView)findViewById(R.id.imgLogo)));
                ((TextView)findViewById(R.id.txtTitle)).setText(data.model.base.name);
                ((TextView)findViewById(R.id.txtSubTitle)).setText(data.model.infoLine);
            }
        }
    }

}
