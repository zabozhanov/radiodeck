package com.radiodeck.net;

import com.radiodeck.net.responses.BrowseResponse;
import com.radiodeck.net.responses.radio.RadioResponse;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.Path;

/**
 * Created by zabozhanov on 27/10/14.
 */
public interface ApiService {

    @Headers({"X-RadioDeck-Source: iOS", "X-RadioDeck-Content: DEV"})
    @GET("/api/v1/find?genre=music&extras=genres")
    public BrowseResponse getMusic();

    @Headers({"X-RadioDeck-Source: iOS", "X-RadioDeck-Content: DEV"})
    @GET("/{next}")
    public BrowseResponse getMusicNext(@Path(value = "next", encode = false) String url);

    @Headers("X-RadioDeck-Source: ANDROID")
    @GET("/api/v1/discover/")
    public BrowseResponse getBrowse();

    @Headers({"X-RadioDeck-Source: iOS", "X-RadioDeck-Content: DEV"})
    @GET("/api/v1/radio/{id}")
    public RadioResponse getRadio(@Path("id") String id);



}
