package com.radiodeck.net.responses;

import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zabozhanov on 04/11/14.
 */
public class BrowseExtra {
    public List<JsonObject> moreData;
    public DiscoverCampaignsTop campaignsTop;
    public BrowseExtra() {
        moreData = new ArrayList<JsonObject>();
        campaignsTop = new DiscoverCampaignsTop();
    }
}
