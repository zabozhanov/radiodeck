package com.radiodeck.net.responses;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zabozhanov on 04/11/14.
 */
public class BrowseModel {
    public int listSize;
    public int total;
    public int offset;
    public int limit;
    public String next;
    public List<Station> list;

    public BrowseModel() {
        list = new ArrayList<Station>();
    }
}
