package com.radiodeck.net.responses;

/**
 * Created by zabozhanov on 27/10/14.
 */
public class BrowseResponse extends BaseResponse{

    public BrowseExtra extra;
    public BrowseModel model;

    public BrowseResponse() {
        extra = new BrowseExtra();
        model = new BrowseModel();
    }
}
