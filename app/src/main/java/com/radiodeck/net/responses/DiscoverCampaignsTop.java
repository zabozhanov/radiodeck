package com.radiodeck.net.responses;

import java.util.List;

/**
 * Created by zabozhanov on 29/10/14.
 */
public class DiscoverCampaignsTop {
    public List<DiscoverCampaign> list;
    public String type;
}
