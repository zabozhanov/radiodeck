package com.radiodeck.net.responses;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zabozhanov on 28/10/14.
 */
public class DiscoverData {

    public String title;
    public String display;
    public String type;
    public List<Station> list;

    public DiscoverData() {
        list = new ArrayList<Station>();
        title = display = type = "";
    }
}
