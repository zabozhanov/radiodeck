package com.radiodeck.net.responses;

/**
 * Created by zabozhanov on 28/10/14.
 */
public class Station {

    public StationBase base;
    public String infoLine;
    public String _id;

    public Station() {
        base = new StationBase();
        infoLine = _id = "";
    }
}
