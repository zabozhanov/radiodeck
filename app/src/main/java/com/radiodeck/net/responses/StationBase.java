package com.radiodeck.net.responses;

/**
 * Created by zabozhanov on 28/10/14.
 */
public class StationBase {

    public String name;
    public boolean hasLogo;

    public StationBase() {
        name = "";
        hasLogo = false;
    }
}
