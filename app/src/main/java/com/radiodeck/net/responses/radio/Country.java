package com.radiodeck.net.responses.radio;

/**
 * Created by zabozhanov on 02/11/14.
 */
public class Country {
    public String label;
    public String name;
    public String url;

}
