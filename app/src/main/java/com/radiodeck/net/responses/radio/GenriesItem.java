package com.radiodeck.net.responses.radio;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zabozhanov on 02/11/14.
 */
public class GenriesItem {
    public String title;
    public String display;
    public List<Genre> list;

    public GenriesItem() {
        list = new ArrayList<Genre>();
    }
}
