package com.radiodeck.net.responses.radio;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zabozhanov on 02/11/14.
 */
public class LocationItem {
    public String title;
    public String display;
    public List<Country> list;

    public LocationItem() {
        list = new ArrayList<Country>();
    }
}
