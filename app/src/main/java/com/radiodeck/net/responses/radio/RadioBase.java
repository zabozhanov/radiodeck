package com.radiodeck.net.responses.radio;

/**
 * Created by zabozhanov on 01/11/14.
 */
public class RadioBase {
    public String name;
    public boolean hasLogo;
    public boolean hasCover;
}
