package com.radiodeck.net.responses.radio;

import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zabozhanov on 01/11/14.
 */
public class RadioExtra {
    public List<JsonObject> moreData;

    public RadioExtra() {
        moreData = new ArrayList<JsonObject>();
    }

}
