package com.radiodeck.net.responses.radio;

import java.util.ArrayList;
import java.util.List;

import com.radiodeck.net.responses.Station;

/**
 * Created by zabozhanov on 01/11/14.
 */
public class RadioRecommendations {
    public String title;
    public String display;
    public List<Station> list;

    public RadioRecommendations() {
        list = new ArrayList<Station>();
    }
}
