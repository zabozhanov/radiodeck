package com.radiodeck.net.responses.radio;

import com.radiodeck.net.responses.BaseResponse;

/**
 * Created by zabozhanov on 01/11/14.
 */
public class RadioResponse extends BaseResponse {
    public RadioModel model;
    public RadioExtra extra;


    public RadioResponse() {
        model = new RadioModel();
        extra = new RadioExtra();
    }
}
