package com.radiodeck.views;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.radiodeck.R;
import com.squareup.picasso.Picasso;

/**
 * Created by zabozhanov on 04/11/14.
 */
public class CampaignFragment extends Fragment{

    static final String IMAGE_URL = "IMAGE_URL";
    String url;
    protected ImageView imageView;

    public static CampaignFragment newInstance(String url) {
        CampaignFragment pageFragment = new CampaignFragment();
        Bundle arguments = new Bundle();
        arguments.putString(IMAGE_URL, url);
        pageFragment.setArguments(arguments);
        return pageFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        url = getArguments().getString(IMAGE_URL);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.item_campaign_frame, null);
        imageView = (ImageView) view.findViewById(R.id.imgView);

        if (getActivity() != null) {
            Picasso.with(getActivity())
                    .load(url)
                    .into(imageView);
        }

        return view;
    }
}
