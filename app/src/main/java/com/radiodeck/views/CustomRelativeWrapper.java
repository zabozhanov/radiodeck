package com.radiodeck.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * Created by zabozhanov on 01/11/14.
 */
public class CustomRelativeWrapper extends RelativeLayout{
    private int mOffset;

    public CustomRelativeWrapper(Context context) {
        super(context);
    }

    public CustomRelativeWrapper(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomRelativeWrapper(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    protected void dispatchDraw(Canvas canvas) {
        canvas.clipRect(new Rect(getLeft(), getTop(), getRight(), getBottom() - mOffset)); //this will clip the header so our rows don't overlap the header
        super.dispatchDraw(canvas);
    }

    public void setClipY(int offset) {
        mOffset = offset;
        invalidate();
    }
}
