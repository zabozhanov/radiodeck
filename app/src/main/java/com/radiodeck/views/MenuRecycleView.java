package com.radiodeck.views;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.oguzdev.circularfloatingactionmenu.library.FloatingActionMenu;

/**
 * Created by zabozhanov on 07/11/14.
 */
public class MenuRecycleView extends RecyclerView {


    public FloatingActionMenu menu;


    public MenuRecycleView(Context context) {
        super(context);
    }

    public MenuRecycleView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MenuRecycleView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent e) {


        switch (e.getAction()) {
            case MotionEvent.ACTION_UP: // отпускание
            case MotionEvent.ACTION_CANCEL:
                if (menu != null && menu.isOpen()) {
                    menu.close(true);
                }
        }

        return super.onInterceptTouchEvent(e);
    }
}
