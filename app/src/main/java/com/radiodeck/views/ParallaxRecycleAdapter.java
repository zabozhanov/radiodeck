package com.radiodeck.views;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import com.radiodeck.AppModel;
import com.radiodeck.HorizontalRecyclerViewAdapter;
import com.radiodeck.PlayerActivity;
import com.radiodeck.R;
import com.radiodeck.net.responses.Station;
import com.radiodeck.net.responses.radio.Country;
import com.radiodeck.net.responses.radio.Genre;
import com.radiodeck.net.responses.radio.LocationItem;
import com.radiodeck.net.responses.radio.GenriesItem;
import com.radiodeck.net.responses.radio.RadioRecommendations;
import com.radiodeck.net.responses.radio.RadioResponse;


/**
 * Created by zabozhanov on 31/10/14.Ø
 */
public class ParallaxRecycleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    enum ITEM_TYPES {
        ITEM_TYPE_TOP_HEADER, //listview header
        ITEM_TYPE_LIST_FOOTER, //listview footer
        ITEM_TYPE_PLUS_PLAY, //plus-minus item
        ITEM_TYPE_LIST_ITEM, //station item
        ITEM_TYPE_GENRE_LOCATION, //genre-location item
        ITEM_TYPE_COVER_LIST, //hor list
        ITEM_TYPE_LIST_HEADER, //section header
    }

    class RecycleItem {
        public ITEM_TYPES type;
        public String header;
        public int pos;
        public int itemPos;
        public Station station;
        public Genre genre;
        public Country location;
        public boolean isLastItem;
        public boolean isFirstItem;

        public HorizontalRecyclerViewAdapter horizontalRecyclerViewAdapter;

        RecycleItem(ITEM_TYPES type, String header) {
            this.type = type;
            this.header = header;
        }

        RecycleItem(ITEM_TYPES type, int pos) {
            this.type = type;
            this.pos = pos;
        }

        RecycleItem(ITEM_TYPES type, int pos, int itemPos) {
            this.itemPos = itemPos;
            this.type = type;
            this.pos = pos;
        }
    }

    public CustomRelativeWrapper mHeader;
    protected Context context;
    protected RecyclerView recyclerView;
    protected RadioResponse data;
    protected List<RecycleItem> recycleItems;
    public ImageView changeSizeHeaderImageView;

    public ParallaxRecycleAdapter(Context context, RecyclerView recyclerView) {
        this.context = context;
        this.recyclerView = recyclerView;
        data = new RadioResponse();
        recycleItems = new ArrayList<RecycleItem>();
    }

    public void updateWithData(RadioResponse data) {
        if (data == null) {
            return;
        }
        this.data = data;

        this.data = data;
        recycleItems = new ArrayList<RecycleItem>();
        int pos = 0;

        recycleItems.add(new RecycleItem(ITEM_TYPES.ITEM_TYPE_TOP_HEADER, 0));
        recycleItems.add(new RecycleItem(ITEM_TYPES.ITEM_TYPE_PLUS_PLAY, 0));

        Gson gson = new Gson();
        for (JsonObject recommendation : data.extra.moreData) {
            if (recommendation.get("type").toString().equals("\"STATION\"")) {
                RadioRecommendations recommendations = gson.fromJson(recommendation.toString(), RadioRecommendations.class);
                if (recommendations.display.equals("LIST")) {
                    //cover = false;
                    recycleItems.add(new RecycleItem(ITEM_TYPES.ITEM_TYPE_LIST_HEADER, recommendations.title));
                    for (int i = 0; i < recommendations.list.size(); i++) {
                        RecycleItem item = new RecycleItem(ITEM_TYPES.ITEM_TYPE_LIST_ITEM, pos, i);
                        item.station = recommendations.list.get(i);
                        recycleItems.add(item);
                    }
                    recycleItems.get(recycleItems.size() - 1).isLastItem = true;
                } else {
                    RecycleItem item = new RecycleItem(ITEM_TYPES.ITEM_TYPE_COVER_LIST, pos);
                    item.header = recommendations.title;
                    item.horizontalRecyclerViewAdapter = new HorizontalRecyclerViewAdapter(context, recyclerView);
                    item.horizontalRecyclerViewAdapter.updateWithData(recommendations.list);
                    recycleItems.add(item);
                }
            } else if (recommendation.get("type").toString().equals("\"GENRE\"")) {
                GenriesItem genries = gson.fromJson(recommendation.toString(), GenriesItem.class);
                recycleItems.add(new RecycleItem(ITEM_TYPES.ITEM_TYPE_LIST_HEADER, genries.title));
                for (int i = 0; i < genries.list.size(); i++) {
                    RecycleItem item = new RecycleItem(ITEM_TYPES.ITEM_TYPE_GENRE_LOCATION, pos, i);
                    if (i == 0) item.isFirstItem = true;
                    item.genre = genries.list.get(i);
                    recycleItems.add(item);
                }
                recycleItems.get(recycleItems.size() - 1).isLastItem = true;
            } else if (recommendation.get("type").toString().equals("\"LOCATION\"")) {
                LocationItem locationItem = gson.fromJson(recommendation.toString(), LocationItem.class);
                recycleItems.add(new RecycleItem(ITEM_TYPES.ITEM_TYPE_LIST_HEADER, locationItem.title));
                for (int i = 0; i < locationItem.list.size(); i++) {
                    RecycleItem item = new RecycleItem(ITEM_TYPES.ITEM_TYPE_GENRE_LOCATION, pos, i);
                    if (i == 0) item.isFirstItem = true;
                    item.location = locationItem.list.get(i);
                    recycleItems.add(item);
                }
                recycleItems.get(recycleItems.size() - 1).isLastItem = true;
            }
            pos++;
        }
        recycleItems.add(new RecycleItem(ITEM_TYPES.ITEM_TYPE_LIST_FOOTER, 0));

        notifyDataSetChanged();
    }

    public void translateHeader(float of) {
        mHeader.setTranslationY(of);
        mHeader.setClipY(Math.round(of));
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        if (i == ITEM_TYPES.ITEM_TYPE_TOP_HEADER.ordinal()) { //if viewtype==1 then it's the header
            return new ViewHolderParallaxHeader(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.station_header_view, viewGroup, false));
        } else if (i == ITEM_TYPES.ITEM_TYPE_PLUS_PLAY.ordinal()) { //plus play row
            return new ViewHolderPlusPlay(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.station_play_plus_row, viewGroup, false));
        } else if (i == ITEM_TYPES.ITEM_TYPE_LIST_HEADER.ordinal()) { //stations header row
            return new ViewHolderHeader(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_station_header_with_divider, viewGroup, false));
        } else if (i == ITEM_TYPES.ITEM_TYPE_LIST_ITEM.ordinal()) { //stations item row
            return new ViewHolderListStation(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_station_list_item, viewGroup, false));
        } else if (i == ITEM_TYPES.ITEM_TYPE_COVER_LIST.ordinal()) { //horizontal list
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_cover_listview, viewGroup, false);
            return new ViewHolderCoverItem(v);
        } else if (i == ITEM_TYPES.ITEM_TYPE_LIST_FOOTER.ordinal()) { //footer
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_footer, viewGroup, false);
            v.setBackgroundColor(context.getResources().getColor(R.color.gray_background));
            return new ViewHolder(v);
        } else if (i == ITEM_TYPES.ITEM_TYPE_GENRE_LOCATION.ordinal()) { //country location
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_country_genry, viewGroup, false);
            return new ViewHolderGenreLocation(v);
        } else {
            return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_station_list_item, viewGroup, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {

        RecycleItem item = recycleItems.get(i);

        if (item.type == ITEM_TYPES.ITEM_TYPE_TOP_HEADER) { //header
            ViewHolderParallaxHeader viewHolderHeader = (ViewHolderParallaxHeader) viewHolder;
            Picasso.with(context).load(AppModel.getInstance().
                    getLogoUrl(data.model._id, data.model.base.hasLogo, false))
                    .into(viewHolderHeader.logoView);
            viewHolderHeader.txtTitle.setText(data.model.base.name);
            viewHolderHeader.txtSubTitle.setText(data.model.infoLine);

        } else if (item.type == ITEM_TYPES.ITEM_TYPE_PLUS_PLAY) { //plus play

        } else if (item.type == ITEM_TYPES.ITEM_TYPE_LIST_FOOTER) { //plus play

        } else if (item.type == ITEM_TYPES.ITEM_TYPE_LIST_HEADER) { //station heaader
            ViewHolderHeader viewHolderHeader = (ViewHolderHeader) viewHolder;
            viewHolderHeader.textView.setText(item.header);
        } else if (item.type == ITEM_TYPES.ITEM_TYPE_COVER_LIST) {
            ViewHolderCoverItem viewHolderCoverItem = (ViewHolderCoverItem) viewHolder;
            viewHolderCoverItem.txtTitle.setText(item.header);
            if (viewHolderCoverItem.hListView.getAdapter() == null) {
                viewHolderCoverItem.hListView.setAdapter(item.horizontalRecyclerViewAdapter);
                viewHolderCoverItem.hListView.getAdapter().notifyDataSetChanged();
            }
        } else if (item.type == ITEM_TYPES.ITEM_TYPE_GENRE_LOCATION) { //station heaader
            ViewHolderGenreLocation viewHolderGenreLocation = (ViewHolderGenreLocation) viewHolder;
            if (item.location != null) {
                viewHolderGenreLocation.txtTitle.setText(item.location.label);
            } else if (item.genre != null) {
                viewHolderGenreLocation.txtTitle.setText(item.genre.label);
            }
            viewHolderGenreLocation.dividerView.setVisibility(item.isLastItem ? View.INVISIBLE : View.VISIBLE);
            float scale = context.getResources().getDisplayMetrics().density;
            int dpAsPixels = (int) ((item.isFirstItem ? 0 : 10) * scale + 0.5f);
            viewHolderGenreLocation.txtTitle.setPadding(0, dpAsPixels, 0, (int) (10 * scale + 0.5f));
        } else {
            ViewHolderListStation viewHolderListStation = (ViewHolderListStation) viewHolder;
            viewHolderListStation.txtSubTitle.setText(item.station.infoLine);
            viewHolderListStation.txtTitle.setText(item.station.base.name);
            Picasso.with(context).load(AppModel.getInstance().
                    getLogoUrl(item.station._id, item.station.base.hasLogo, false))
                    .into(viewHolderListStation.imgView);
            viewHolderListStation.dividerView.setVisibility(item.isLastItem ? View.INVISIBLE : View.VISIBLE);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return recycleItems.get(position).type.ordinal();
    }

    @Override
    public int getItemCount() {
        return recycleItems.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View itemView) {
            super(itemView);
            //textView = (TextView) itemView.findViewById(R.id.textView);
        }
    }

    public class ViewHolderParallaxHeader extends RecyclerView.ViewHolder {

        private ImageView backgroundView;
        private ImageView logoView;
        public TextView txtTitle;
        public TextView txtSubTitle;

        public ViewHolderParallaxHeader(View itemView) {
            super(itemView);
            backgroundView = (ImageView) itemView.findViewById(R.id.imgView);
            logoView = (ImageView) itemView.findViewById(R.id.imgLogo);
            txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
            txtSubTitle = (TextView) itemView.findViewById(R.id.txtSubTitle);
        }
    }

    class ViewHolderHeader extends RecyclerView.ViewHolder {
        private TextView textView;

        public ViewHolderHeader(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.textview);
            textView.setTextColor(context.getResources().getColor(android.R.color.black));
        }
    }

    class ViewHolderPlusPlay extends RecyclerView.ViewHolder {
        private ImageButton btnAdd;
        private ImageButton btnPlay;

        public ViewHolderPlusPlay(View itemView) {
            super(itemView);
            btnAdd = (ImageButton) itemView.findViewById(R.id.btnAdd);
            btnPlay = (ImageButton) itemView.findViewById(R.id.btnPlay);
            btnAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    btnAdd.setSelected(!btnAdd.isSelected());
                }
            });
            btnPlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    btnPlay.setSelected(!btnPlay.isSelected());
                    Intent intent = new Intent(context, PlayerActivity.class);
                    intent.putExtra("id", data.model.base.hasLogo ? data.model._id : "");
                    intent.putExtra("title", data.model.base.name);
                    context.startActivity(intent);
                }
            });
        }
    }

    class ViewHolderListStation extends RecyclerView.ViewHolder {
        private TextView txtTitle;
        private TextView txtSubTitle;
        private ImageView imgView;
        private View dividerView;

        public ViewHolderListStation(View itemView) {
            super(itemView);
            txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
            txtSubTitle = (TextView) itemView.findViewById(R.id.txtSubTitle);
            imgView = (ImageView) itemView.findViewById(R.id.imgView);
            dividerView = itemView.findViewById(R.id.horizontaldivider);
        }
    }

    class ViewHolderGenreLocation extends RecyclerView.ViewHolder {
        private TextView txtTitle;
        private View dividerView;

        public ViewHolderGenreLocation(View itemView) {
            super(itemView);
            txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
            dividerView = itemView.findViewById(R.id.horizontaldivider);
        }
    }

    class ViewHolderCoverItem extends RecyclerView.ViewHolder {
        private TextView txtTitle;
        private RecyclerView hListView;
        private LinearLayoutManager layoutManager;

        public ViewHolderCoverItem(View itemView) {
            super(itemView);
            txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
            hListView = (RecyclerView) itemView.findViewById(R.id.horizontalrecycler);
            layoutManager = new LinearLayoutManager(itemView.getContext());
            layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            hListView.setLayoutManager(layoutManager);
        }
    }

}
