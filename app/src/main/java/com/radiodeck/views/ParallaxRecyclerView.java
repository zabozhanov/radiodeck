package com.radiodeck.views;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

/**
 * Created by zabozhanov on 31/10/14.
 */
public class ParallaxRecyclerView extends RecyclerView{

    public OnScrollListener scrollListener;

    public ParallaxRecyclerView(Context context) {
        super(context);
        init();
    }

    public ParallaxRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ParallaxRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                ViewHolder holder = findViewHolderForPosition(0);
                if (holder != null)
                    ((ParallaxRecycleAdapter) getAdapter()).translateHeader(holder.itemView.getTop() * 0.5f);
                if (scrollListener != null) {
                    scrollListener.onScrolled(recyclerView, dx, dy);
                }
            }
        });
    }
}
